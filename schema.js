const {
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLSchema,
    GraphQLList,
    GraphQLNonNull
} = require('graphql');

const axios = require('axios');

//  Hardcored data

/* const customersData = [
    {id:'1', name:'Carlos Penalba', email:'iam@mail.com', age:35},
    {id:'2', name:'Igor Zagaev', email:'zagaev@mail.com', age:25},
    {id:'3', name:'Vila Balek', email:'balek@mail.com', age:32},
];
 */

//  Customer Type

const customerType = new GraphQLObjectType({
    name: 'Customer',
    fields:() => ({
        id: {type:GraphQLString},
        name: {type: GraphQLString},
        email: {type: GraphQLString},
        age: {type: GraphQLInt},
    })
});

//  Root Query

const rootQuery = new GraphQLObjectType({
    name: 'RootQueryType',
    fields: {
        customer : {
            type: customerType,
            args: {
                id: {type: GraphQLString}
            },
            resolve(parentValue, args){
                /* for(let i = 0;i < customersData.length;i++){
                    if(customersData[i].id == args.id){
                        return customersData[i];
                    }
                } */

                return axios.get('http://localhost:3000/customersData/' + args.id)
                .then(response => response.data);
            }
        },
        customers : {
            type: new GraphQLList(customerType),
            resolve(parentValue, args){
                return axios.get('http://localhost:3000/customersData/')
                .then(response => response.data);
            }
        }
    }
});

const mutation = new GraphQLObjectType({
    name: 'mutation',
    fields: {
        addCustomer: {
            type: customerType,
            args: {
                name: {type: new GraphQLNonNull(GraphQLString)},
                email: {type: new GraphQLNonNull(GraphQLString)},
                age: {type: new GraphQLNonNull(GraphQLInt)}
            },
            resolve(parentValue, args){
                return axios.post('http://localhost:3000/customersData', {
                    name: args.name,
                    email: args.email,
                    age: args.age
                })
                .then(response => response.data);
            }
        },
        deleteCustomer: {
            type: customerType,
            args: {
                id: {type: new GraphQLNonNull(GraphQLString)}
            },
            resolve(parentValue, args){
                return axios.delete('http://localhost:3000/customersData/' + args.id)
                .then(response => response.data);
            }
        },
        updateCustomer: {
            type: customerType,
            args: {
                id: {type: new GraphQLNonNull(GraphQLString)},
                name: {type: GraphQLString},
                email: {type: GraphQLString},
                age: {type: GraphQLString}
            },
            resolve(parentValue, args){
                return axios.patch('http://localhost:3000/customersData/' + args.id, args)
                .then(response => response.data);
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: rootQuery,
    mutation
});